//
//  MapViewController.swift
//  LocateMe
//
//  Created by Ashley Jain on 6/30/17.
//  Copyright © 2017 workshop. All rights reserved.
//

import UIKit
import ArcGIS

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: AGSMapView!
    private var portal: AGSPortal!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("map")
        self.mapView.map = AGSMap(basemapType: .imageryWithLabelsVector, latitude: 34.057, longitude: -117.196, levelOfDetail: 17)
        
        self.portal = AGSPortal(url: URL(string: "http://uofmd.maps.arcgis.com/")!, loginRequired: false)
        self.portal.credential = AGSCredential(user: "maracai77", password: "locus4160")
        self.portal.load() {[weak self] (error) in
            if let error = error {
                print(error)
                return
            }
            // check the portal item loaded and print the modified date
            if self?.portal.loadStatus == AGSLoadStatus.loaded {
                let fullName = self?.portal.user?.fullName
                print(fullName!)
            }
        }
        
        // create a PortalItem based on a pre-defined portal id
        let portalItem = AGSPortalItem(portal: portal, itemID: "30010341c0d3439ab3337cf79d70dc6b")
        // create a map from a PortalItem
        let map = AGSMap(item: portalItem)
        
        let mapImageLayer = AGSArcGISMapImageLayer(url: URL(string: "https://gis.fm.umd.edu/arcgis/rest/services/InteriorSpace/GISFloorplansALL/MapServer")!)
        mapImageLayer.credential = AGSCredential(user: "agrawala", password: "aa1234")
        map.operationalLayers.add(mapImageLayer)
        
        // set the map to be displayed in an AGSMapView
        self.mapView.map = map
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func wifiAction(_ sender: Any) {
        //POST request for response
        let myUrl = URL(string: "http://rovermind.cs.umd.edu:8080/LocationServer/FindLocation?type=ap");
        
        var request = URLRequest(url:myUrl!)
        
        request.httpMethod = "POST"// Compose a query string
        
        let postString2 = "<?xml version=\"1.0\"?><data><deviceid>65738392137234</deviceid><accesspoints><accesspoint><name>umd</name><mac>00:81:c4:1e:09:7f</mac><signal>-58</signal><freq>5765</freq></accesspoint></accesspoints></data>";
        
        request.httpBody = postString2.data(using: String.Encoding.utf8);
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil
            {
                print(error)
                return
            }
            // You can print out response object
            print(response)
            let responseString = String(data: data!,encoding: String.Encoding.utf8)
            print(responseString!)
            
            let mapPoint = AGSPoint(x: -76.936349, y: 38.990361, spatialReference: AGSSpatialReference.wgs84())

            self.mapView.setViewpoint(AGSViewpoint(center: mapPoint, scale: 700), duration: 1, curve: AGSAnimationCurve.easeInOutSine, completion:  nil);
            
            
            if(self.mapView.callout.isHidden == false){
                self.mapView.callout.dismiss()
            }
            
            self.mapView.callout.detail = responseString!
            self.mapView.callout.isAccessoryButtonHidden = true
            self.mapView.callout.show(at: mapPoint, screenOffset: CGPoint.zero, rotateOffsetWithMap: false, animated: true)
        }
        task.resume()
    }

    @IBAction func gpsAction(_ sender: Any) {
    
        self.mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanMode.recenter
        self.mapView.locationDisplay.start { (error:Error?) -> Void in
            if let error = error {
                print(error)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
