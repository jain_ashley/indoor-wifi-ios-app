//
//  StartViewController.swift
//  LocateMe
//
//  Created by Ashley Jain on 6/29/17.
//  Copyright © 2017 workshop. All rights reserved.
//

import UIKit
import SystemConfiguration.CaptiveNetwork


class StartViewController: UIViewController {

    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var locateMeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func wifiscan(_ sender: Any) {
        
        locateMeButton.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.locateMeButton.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
        
        print("pressed")
        
        var postString:String
        postString = "<?xml version=\"1.0\"?><data><deviceid>65738392137234</deviceid><accesspoints><accesspoint><name>"
        
        var SSID:String
        var BSSID:String
        //wifi ssid and bssid
        let interfaces = CNCopySupportedInterfaces()
        if interfaces != nil {
            let interfacesArray = interfaces as! [String]
            let interfaceName = interfacesArray[0] as String
            let unsafeInterfaceData =     CNCopyCurrentNetworkInfo(interfaceName as CFString)
            var interfaceData = unsafeInterfaceData as! Dictionary <String,AnyObject>
            SSID = (interfaceData["SSID"] as? String)!
            postString = postString + SSID
            postString = postString + "</name><mac>"

            BSSID = (interfaceData["BSSID"] as? String)!
            print(BSSID)
            postString = postString + BSSID
            postString = postString + "</mac><signal>-58</signal><freq>5765</freq></accesspoint></accesspoints></data>"
            print(postString)
    
        }
        
        
        
    
        //POST request for response
        let myUrl = URL(string: "http://rovermind.cs.umd.edu:8080/LocationServer/FindLocation?type=ap");
        
        var request = URLRequest(url:myUrl!)
        
        request.httpMethod = "POST"// Compose a query string
        
        let postString2 = "<?xml version=\"1.0\"?><data><deviceid>65738392137234</deviceid><accesspoints><accesspoint><name>umd</name><mac>00:81:c4:1e:09:7f</mac><signal>-58</signal><freq>5765</freq></accesspoint></accesspoints></data>";
        
        request.httpBody = postString2.data(using: String.Encoding.utf8);

        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil
            {
                print(error)
                return
            }
            // You can print out response object
            print(response)
            let responseString = String(data: data!,encoding: String.Encoding.utf8)
            print(responseString!)
            
            self.text.text = responseString
        }
        task.resume()
        
    }

}

