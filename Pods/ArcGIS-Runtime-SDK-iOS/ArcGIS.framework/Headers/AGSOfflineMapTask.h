/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

@class AGSMap;
@class AGSPortalItem;
@class AGSGenerateOfflineMapJob;
@class AGSGenerateOfflineMapParameters;
@class AGSOfflineMapCapabilities;

#import "AGSLoadableBase.h"

/** @file AGSOfflineMapTask.h */ //Required for Globals API doc

/** @brief A task to take a map offline
 
 Instances of this class represent a task that can be used to take a map offline. Taking a map offline involves downloading the map's metadata, its constituent layers, and their data.
 
 @since 100.1
 */
@interface AGSOfflineMapTask : AGSLoadableBase

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize the task with the provided map to take offline. The map must be a webmap either on ArcGIS Online or an on-premises ArcGIS Portal.
 @param onlineMap to take offline
 @return a new intitalized task
 @since 100.1
 */
-(instancetype)initWithOnlineMap:(AGSMap *)onlineMap;

/** Initialize the task with the provided portal item. The item must represent a webmap (item type should be `AGSPortalItemTypeWebMap`) either on ArcGIS Online or an on-premises ArcGIS Portal.
 @param portalItem specifying a map to take offline
 @return a new intitalized task
 @since 100.1
 */
-(instancetype)initWithPortalItem:(AGSPortalItem *)portalItem;

/** Initialize the task with the provided map to take offline. The map must be a webmap either on ArcGIS Online or an on-premises ArcGIS Portal.
 @param onlineMap to take offline
 @return a new intitalized task
 @since 100.1
 */
+(instancetype)offlineMapTaskWithOnlineMap:(AGSMap *)onlineMap;

/** Initialize the task with the provided portal item. The item must represent a webmap (item type should be `AGSPortalItemTypeWebMap`) either on ArcGIS Online or an on-premises ArcGIS Portal.
 @param portalItem specifying a map to take offline
 @return a new intitalized task
 @since 100.1
 */
+(instancetype)offlineMapTaskWithPortalItem:(AGSPortalItem *)portalItem;

#pragma mark -
#pragma mark properties

/** The map to take offline. The map must be a webmap either on ArcGIS Online or an on-premises ArcGIS Portal.
 @since 100.1
 */
@property (nullable, nonatomic, strong, readonly) AGSMap *onlineMap;

/** Portal item specifying the map to take offline. The item must represent a webmap (item type should be `AGSPortalItemTypeWebMap`) either on ArcGIS Online or an on-premises ArcGIS Portal.
 @since 100.1
 */
@property (nullable, nonatomic, strong, readonly) AGSPortalItem *portalItem;

#pragma mark -
#pragma mark methods

/** Returns a job which can be used to generate an offline map using the specified parameters. The result of the job will be of type `AGSGenerateOfflineMapResult`.
 @note The job is dormant and needs to be explicitly started using `AGSGenerateOfflineMapJob#startWithStatusHandler:completion:`
 @param parameters specifying how to take a map offline
 @param downloadDirectory where the offline map should be saved on disk. If the directory exists it should be empty otherwise the operation will fail. If the directory doesn't exist, it will be created by the job.
 @return job to generate an offline map. The result of the job will be of type `AGSGenerateOfflineMapResult`.
 @since 100.1
 */
-(AGSGenerateOfflineMapJob *)generateOfflineMapJobWithParameters:(AGSGenerateOfflineMapParameters *)parameters
                                               downloadDirectory:(NSURL *)downloadDirectory;

/** A convenience method to get properly initialized parameters for generating an offline map.
 
 This will populate the parameters with values matching what the service supports. For example if the service does not support `PER_LAYER` sync  model then `PER_GEODATABASE` will be used. All layers from the service will be included. The extent will be the service's full extent. Attachments are included by default, but related tables/layers are not included.
 
 The returned parameter's min scale and max scale are zero and that'll include tiles for the full range of scales the tiled layers support when taking the map offline. The  `itemInfo` property of the parameter is initialized based this task's portal item, if that is set, and will include the item's thumbnail.
 
 @param areaOfInterest An `AGSPolygon` or `AGSEnvelope` geometry that defines the geographic area for which the map data should be taken offline 
 @param completion block that is invoked with the initialized params if the method succeeds, or an error if it fails
 @return operation that can be canceled
 @since 100.1
 */
-(id<AGSCancelable>)defaultGenerateOfflineMapParametersWithAreaOfInterest:(AGSGeometry *)areaOfInterest
                                                               completion:(void(^)(AGSGenerateOfflineMapParameters * __nullable params, NSError * __nullable error))completion;

/** Provides information about offline capabilities of the map, such as which layers can be taken offline.
 @param parameters specifying how to take the map offline
 @param completion block that is invoked with offline capabilities of the map if the operation succeeds, or an error if it fails
 @return operation that can be canceled
 @since 100.1
 */
-(id<AGSCancelable>)getOfflineMapCapabilitiesWithParameters:(AGSGenerateOfflineMapParameters *)parameters
                                                 completion:(void(^)(AGSOfflineMapCapabilities * __nullable params, NSError * __nullable error))completion;

@end

NS_ASSUME_NONNULL_END
