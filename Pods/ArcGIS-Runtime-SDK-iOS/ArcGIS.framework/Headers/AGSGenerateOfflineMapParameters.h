/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

@class AGSGeometry;
@class AGSOfflineMapItemInfo;

/** @file AGSGenerateOfflineMapParameters.h */ //Required for Globals API doc

/** @brief Parameters to take a map offline
 
 Instances of this class represent parameters that are used with `AGSOfflineMapTask#generateOfflineMapJobWithParameters:completion:` to take a map offline.
 
 @see `AGSOfflineMapTask#defaultGenerateOfflineMapParametersWithAreaOfInterest:completion:` convenience method to get parameters that are initialized with appopriate default values for a map.
 @since 100.1
 */
@interface AGSGenerateOfflineMapParameters : AGSObject

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize the parameters with the provided information for taking a map offline.
 @param areaOfInterest  An `AGSPolygon` or `AGSEnvelope` geometry that defines the geographic area for which the map data (features and tiles) should be taken offline
 @param minScale The minimum scale for which map tiles are needed. There won't be any tiles offline when the map is zoomed out beyond this scale.
 @param maxScale The maximum scale for which map tiles are needed. There won't be any tiles offline when the map is zoomed in beyond this scale.
 @return initialized parameters
 @since 100.1
 */
-(instancetype)initWithAreaOfInterest:(AGSGeometry*)areaOfInterest minScale:(double)minScale maxScale:(double)maxScale;

+(instancetype)generateOfflineMapParameters;

/** Initialize the parameters with the provided information for taking a map offline.
 @param areaOfInterest  An `AGSPolygon` or `AGSEnvelope` geometry that defines the geographic area for which the map data (features and tiles) should be taken offline
 @param minScale The minimum scale for which map tiles are needed. There won't be any tiles offline when the map is zoomed out beyond this scale.
 @param maxScale The maximum scale for which map tiles are needed. There won't be any tiles offline when the map is zoomed in beyond this scale.
 @return initialized parameters
 @since 100.1
 */
+(instancetype)generateOfflineMapParametersWithAreaOfInterest:(AGSGeometry*)areaOfInterest minScale:(double)minScale maxScale:(double)maxScale;


#pragma mark -
#pragma mark properties

/** An `AGSPolygon` or `AGSEnvelope` geometry that defines the geographic area for which the map data (features and tiles) should be taken offline
 @since 100.1
 */
@property (nonatomic, strong, readwrite) AGSGeometry *areaOfInterest;

/** The maximum scale for which map tiles are needed. There won't be any tiles when the map is zoomed in beyond this scale. A value of 0 means there is no maximum scale threshold and that all tiles present upto the layer's smallest scale will be available offline.
 @since 100.1
 */
@property (nonatomic, assign, readwrite) double maxScale;

/** The minimum scale for which map tiles are needed. There won't be any tiles when the map is zoomed out beyond this scale. A value of 0 means there is no minimum scale threshold and that all tiles present upto the layer's largest scale will available offline.
 @since 100.1
 */
@property (nonatomic, assign, readwrite) double minScale;

/** Metadata about the map that should be persisted when it is taken offline. When using the convenience method `AGSOfflineMapTask#defaultGenerateOfflineMapParametersWithAreaOfInterest:completion:` to get the default parameters, this metadata is initialized based on the map's portal item.
 @since 100.1
 */
@property (nullable, nonatomic, strong, readwrite) AGSOfflineMapItemInfo *itemInfo;

/** Specifies how sync-enabled feature layers in the offline map should be configured to sync attachment data with their originating service
 @since 100.1
 */
@property (nonatomic, assign, readwrite) AGSAttachmentSyncDirection attachmentSyncDirection;

/** Specifies whether the basemap should be included when the map is taken offline.
 @since 100.1
 */
@property (nonatomic, assign, readwrite) BOOL includeBasemap;

/** Specifies whether or not to include attachments for feature layers when taking the map offline
 @since 100.1
 */
@property (nonatomic, assign, readwrite) AGSReturnLayerAttachmentOption returnLayerAttachmentOption;

/** Specifies whether to only include the schema or also include data for feature layers when taking the map offline.
 @since 100.1
 */
@property (nonatomic, assign, readwrite) BOOL returnSchemaOnlyForEditableLayers;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
